#!/bin/bash

########################################################
#
#
# Script to measure latencey between X and ETH Zurich
# Author christian.bolliger@id.ethz.ch
#
#
########################################################

######################################################################
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation version 3 of the License. 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <https://www.gnu.org/licenses/>.
#
#####################################################################

# Target variables assuming default control port 12685 
TARGET=latency.ethz.ch
DATAPORT=50001

# Setting random offset of dataport to avoid collisions
DATAPORT=$(( DATAPORT + RANDOM % 100 ))

# Output file w/o suffix
NETPERF=latency-`hostname`

# Benchmark variables
TTIME=60
ITER=10
PAUSE=30
TESTOPTS=min_latency,p50_latency,p90_latency,max_latency,stddev_latency,transaction_rate 

# Getting local IP
function getip () {
if command -v ip &> /dev/null 
then
    IP=`ip addr | egrep -o "inet [[:digit:]]{1,3}\.[[:digit:]]{1,3}\.[[:digit:]]{1,3}\.[[:digit:]]{1,3}" \
    | grep -v 127\.0\.0\.. | cut -d " " -f 2`

elif command -v ifconfig  ip &> /dev/null 
then
    IP=`ifconfig | egrep -o "inet [[:digit:]]{1,3}\.[[:digit:]]{1,3}\.[[:digit:]]{1,3}\.[[:digit:]]{1,3}" \
    | grep -v 127\.0\.0\.. | cut -d " " -f 2`
else
    echo "Could not determine your IP, please enter it manually"
    read IP
fi
echo $IP
}
IPADDR="$(getip)"

echo "Latency measurement to ETH Zurich"
echo "================================="

echo "LATENCY TEST to ETH Zurich" > $NETPERF.out
if ! command -v netperf  &> /dev/null 
then
    echo "ERROR"
    echo "Requires netperf to be installed and in the path"
    exit 1
fi
echo "Test parameters"
echo "Target host: $TARGET" | tee -a $NETPERF.out
echo "Data port: $DATAPORT" | tee -a $NETPERF.out
echo "Duration of each test: $TTIME" | tee -a $NETPERF.out
echo "Number of iterations: $ITER" | tee -a $NETPERF.out
echo "Pause between tests: $PAUSE" | tee -a $NETPERF.out

ETA=$(( ITER * (TTIME + PAUSE) ))
echo "Estimated run time $ETA s"
echo "Carried out from `hostname`" >> $NETPERF.out
echo "with IP(s): $IPADDR" >> $NETPERF.out
echo >> $NETPERF.out
echo "Test start: `date "+%F %T UTC%z"`" | tee -a $NETPERF.out
echo "#,$TESTOPTS"
echo "" > $NETPERF.tmp
for (( I=1; I<=$ITER; I++ ))
do
    echo -n "$I," | tee -a $NETPERF.tmp
    netperf -l $TTIME -H $TARGET -4 -j -p 12865 -t TCP_RR -P 0 \
       -- -P ,$DATAPORT -o $TESTOPTS \
       | tee -a $NETPERF.tmp
    if [ $I -lt $ITER ]
    then
       sleep $PAUSE
    fi
done
echo "Test end: `date "+%F %T UTC%z"`" | tee -a $NETPERF.out
echo "" >> $NETPERF.out
echo "#,$TESTOPTS" | tee -a $NETPERF.out
sort -t , -n  -k 6 $NETPERF.tmp >> $NETPERF.out
RESULT=`grep -m 1 -e "^[1-9]" $NETPERF.out | cut -d "," -f 3`
TRESULT=`printf "%.2f\n" $(( $RESULT ))e-3`
echo | tee -a $NETPERF.out
echo "The median latency from the test with the lowest standard latency is taken."
echo "Decisive latency: $TRESULT ms." | tee -a $NETPERF.out

echo "Please include $NETPERF.out and $NETPERF.md5 into your offer."

if  command -v md5sum &> /dev/null
then
    md5sum $0 $NETPERF.out > $NETPERF.md5
elif command -v md5 &> /dev/null
then
    md5 $0 $NETPERF.out > $NETPERF.md5

elif command -v openssl  &>/dev/null
then
    openssl dgst -md5 $0 > $NETPERF.out
else
    echo "Could not generate the md5sum of $0 and $NETPERF.out."
    echo "Please provide it manually."
fi
exit 0
