
# Measuring latency from a remote DC to ETH Zurich 

## Description

The bash script `remote-latency.sh` measures the latency from a remote data center (DC) to a dedicated server at ETH Zurich.

## Requirements

The script uses `netperf` to measure latency. Netperf is a lightweight program available for all major Linux distributions. Install it using `apt install netperf` or `dnf install netperf`. Ports for FreeBSD and OpenBSD are available (not tested). For macOS, netperf can be installed using MacPorts (tested) or Homebrew (not tested). After the measurement, the package can be easily removed by the used package manager.

## Usage for the ETH Zurich RFP

To measure the latency for the ETH Zurich RFP "High Performance Computing Colocation Services," install the script and this `README` by cloning the repository:
`git clone https://gitlab.ethz.ch/hpc-public/remote-dc-latency.git` 

Change to the installation directory and ensure that `netperf` is installed and in the path. Call `./remote-latency.sh` without any changes. The tests last about 900 seconds, with intermediate results shown. If the script cannot determine your IP address, you will be prompted to indicate it. Usually, the script should finish without any prompt. The final result will be shown on the screen by the line:

"Decisive latency: xxx ms"

Enter this latency into the RFP spreadsheet (this number can also be found in  the file `latency-<hostname>.out`).

Three files are produced, each containing the hostname of your system in its filename:
- `latency-<hostname>.out`
- `latency-<hostname>.md5`
- `latency-<hostname>.tmp`

Attach the the `latency-<hostname>.out` and `latency-<hostname>.md5` files to your tender. The `*.tmp` file can be discarded.

For the RFP test, you must not change the script! Feel free to adapt it for further tests, which are not part of your tender.

For best results, use a system physically attached to your LAN. WLAN connections add significant additional latency.


## Usage for Your Own Measurements

If you want to use this script to measure the latency between two systems under your control, install `netperf` on both systems. On the target system you need to start `netserver` which is included in the `neperf` package. On the target system the default control port 12865 must be reachable as well as an aribtary data port. It is well hidden in the `netperf` documentation that you need this additional data port for the measurement. 

To measure the latency between two systems under your control, install `netperf` on both systems. On the target system, start `netserver`, which is included in the netperf package. The default control port 12865 and an arbitrary data port must be reachable on the target system. It is well hidden in the `netperf` documentation that you need this additional data port for the measurement.

Adapt at least the follwing variables in the script:
```
TARGET=eu-ft-01.ethz.ch
DATAPORT=50001
```
And comment the random offeset of the DATAPORT:
```
# Setting random offset of dataport to avoid collisions
DATAPORT=$(( DATAPORT + RANDOM % 100 ))
```
DATAPORT is the additional port that needs to be open on the target system.

To adjust the test according to your needs you can adapt the benchmark variables:
```
TTIME=60
ITER=10
PAUSE=30
TESTOPTS=min_latency,p50_latency,max_latency,stddev_latency 
```

## Methodology

The script uses the `TCP_RR` (TCP Request/Response) test, which is part of `netperf`. This test performs the maximum request/response rate between the `netperf` client and the `netserver` server. Subsequently, netperf outputs a statistical aggregate of the measurement during the selected time period.

To get reliable results for the latency measurement, the test is carried out for 60 seconds. After a 30-second pause, the test is repeated until we get 10 measurements. For the decisive measurement, the median value of the test with the lowest standard deviation for latency is selected. This method minimizes the effects of temporary network issues or jitter.

## Further documentation

Documentation for `netperf` can usually be found in the documentation part of the installed packages. If not, it can be downloaded from GitHub: `https://github.com/HewlettPackard/netperf/blob/master/doc/netperf.html`

A nice blog entry on measuring network latency has been published by Google: https://cloud.google.com/blog/products/networking/using-netperf-and-ping-to-measure-network-latency

## License
The script is published under the GNU GENERAL PUBLIC LICENSE v.3.0 https://www.gnu.org/licenses/gpl-3.0.html

